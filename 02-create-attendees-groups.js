// 🚧 WIP
//node create-attendees-groups "$PARENT_GROUP_NAME" "$WORKSHOP_NAME" $MAX_ATTENDEES "$HANDSON_PROJECT_PATH" "$HANDSON_PROJECT_TARGET_NAME"

const IssueHelper = require('@tanooki/gl.cli.js/issues.helper.js').Helper
const GroupsHelper = require('@tanooki/gl.cli.js/groups.helper').Helper
const ProjectsHelper = require('@tanooki/gl.cli.js/projects.helper').Helper

async function exportProject({handsonProjectPath}) {
  try {
    let schedule = await ProjectsHelper.scheduleProjectExport({project: handsonProjectPath})
    console.log(schedule)
    console.log(schedule.get("message")) // should be equal to '202 Accepted'
  } catch (error) {
    console.log("😡", error.message)
    console.log(error.response.status, error.response.statusText)
    console.log(error.response.data)
  }
}

async function checkProjectExport({handsonProjectPath}) {
  try {
    let projectExport = await ProjectsHelper.getProjectExportStatus({project: handsonProjectPath})
    console.log(projectExport.get("id"), projectExport.get("name"), projectExport.get("export_status"))
    // projectExport.get("export_status") should be equal to 'finished'
    //console.log(projectExport.get("_links").api_url)
    //console.log(projectExport.get("_links").web_url)

    return projectExport.get("export_status")

  } catch (error) {
    console.log("😡", error.message)
    console.log(error.response.status, error.response.statusText)
    console.log(error.response.data)
  }

}

async function downloadHandsonProject({handsonProjectPath}) {

  try {
    let downloadResult = await ProjectsHelper.downloadProjectExport({project: handsonProjectPath, file:__dirname+"/handson.gz"})

    console.log(downloadResult) // == true if all is ok

  } catch (error) {
    console.log("😡", error.message)
    console.log(error.response.status, error.response.statusText)
    console.log(error.response.data)
  }

}


// Create the handson project in the group of the attendee
async function importHansonProject({parentGroupName, subGroupName, attendeeGroupName, handsonProjectTargetName}) {

  try {

    let importResult = await ProjectsHelper.importProjectFile({
      file: __dirname+"/handson.gz",
      namespace: `${parentGroupName}/${subGroupName}/${attendeeGroupName}`,
      path: handsonProjectTargetName
    })

    console.log(importResult)

  } catch (error) {
    console.log("😡", error.message)
    console.log(error.response.status, error.response.statusText)
    console.log(error.response.data)
  }

}


async function create_attendees_groups_from_registrations_issues({parentGroupName, subGroupName, maxAttendees, handsonProjectPath, handsonProjectTargetName}) {
  console.log("🎃 maxAttendees -1", (maxAttendees-1))

  // *** schedule export of the codelab project ***
  await exportProject({handsonProjectPath})

  // *** be sure that the export is available
  var condition = false
  while (condition==false) {
    let exportStatus = await checkProjectExport({handsonProjectPath})
    console.log("👋", exportStatus)
    if(exportStatus=="finished") condition = true
  }

  // Download the handson project
  await downloadHandsonProject({handsonProjectPath})


  let parentGroup = await GroupsHelper.getGroupDetails({group:`${parentGroupName}/${subGroupName}`})
  let parent_group_id = parentGroup.get("id")
  console.log("👋", parent_group_id)

  let issues = await IssueHelper.getProjectIssues({project: `${parentGroupName}/${subGroupName}/registrations`})
  let firstTenIssues = issues.reverse().slice(0, (maxAttendees-1))

  // Create a group for every attendee
  for (var member in firstTenIssues) {
    let issue = firstTenIssues[member]
    console.log(issue.get("iid"), issue.get("id"), issue.get("author").username)

    // 🖐️ here, add every subscribed member to the communications project.
    try {
      let projectMember = await ProjectsHelper.addMemberToProject({
        project: `${parentGroupName}/${subGroupName}/communications`,
        userId: issue.get("author").id,
        accessLevel: 30 // dev access
      })
      console.log(projectMember)
    } catch(error) {
      console.log("😡", error.message)
      console.log(error.response.status, error.response.statusText)
      console.log(error.response.data)
    }


    try {
      // Create a group for the current attendee
      let attendeeGroup = await GroupsHelper.createSubGroup({
        parentGroupId: parent_group_id,
        subGroupPath: `grp_${issue.get("author").username}`,
        subGroupName: `grp_${issue.get("author").username}`,
        visibility: "public"
      })
      console.log("📝", attendeeGroup)

      // Add the current attendee as the owner of the group
      try {
        let groupMember = await GroupsHelper.addMemberToGroup({
          group: `${parentGroupName}/${subGroupName}/grp_${issue.get("author").username}`,
          userId: issue.get("author").id,
          accessLevel: 50 // owner access
        })
        console.log("😃", groupMember)

        // Create the handson project in the group of the attendee
        await importHansonProject({
          parentGroupName,
          subGroupName,
          attendeeGroupName: `grp_${issue.get("author").username}`,
          handsonProjectTargetName
        })

        /*
        let importResult = await ProjectsHelper.importProjectFile({
          file: __dirname+"/handson.gz",
          namespace: `${parentGroupName}/${subGroupName}/grp_${issue.get("author").username}`,
          path: handsonProjectTargetName
        })
        console.log(importResult)
        */

        // Create a "hello" project in the group of the attendee
        /*
        try {
          let project = await ProjectsHelper.createProject({
            name: "hello",
            path: "hello",
            nameSpaceId: attendeeGroup.get("id"),
            initializeWithReadMe: true,
            visibility: "public"
          })
        } catch(error) {
          console.log("😡", error.message)
          console.log(error.response.status, error.response.statusText)
          console.log(error.response.data)
        }
        */

      } catch(error) {
        console.log("😡", error.message)
        console.log(error.response.status, error.response.statusText)
        console.log(error.response.data)
      }

    } catch(error) {
        console.log("😡", error.message)
        console.log(error.response.status, error.response.statusText)
        console.log(error.response.data)
    }

  }
}

/*
create_attendees_groups_from_registrations_issues({
  parentGroupName: "tanooki-workshops",
  subGroupName: "WKS-DEMO-07",
  maxAttendees: 10,
  handsonProjectPath: "tanooki-workshops/workshops/hands-on-demo/amazing-project",
  handsonProjectTargetName: "amazing-project"
})
*/

create_attendees_groups_from_registrations_issues({
  parentGroupName: process.argv[2],
  subGroupName: process.argv[3],
  maxAttendees: process.argv[4],
  handsonProjectPath: process.argv[5],
  handsonProjectTargetName: process.argv[6]
})
